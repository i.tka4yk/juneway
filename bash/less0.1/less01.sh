#!/bin/bash

rm -r {a{1..4},b{1..2},b5,cc} 2>/dev/null
mkdir {a{1..4},b{1..2},b5,cc}

touch {a{1..4}/file{1..8}.txt,b{1..2}/file{9..20}.txt,b5/file-s{20..40}.log,cc/res-{4..7}.tmp}

find */ -type f  -not -path "cc/*" | wc -l > count.file

# count.file must be contain a 77
