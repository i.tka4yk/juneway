#!/bin/bash

function destroyDirStructure() {
    rm -r {a{1..4},b{1..2},b5,cc} 2>/dev/null
}

function madeDirStructure() {
    mkdir {a{1..4},b{1..2},b5,cc}
}

function fillDirStructure() {
    eval touch {a{1..4}/file{${f[0]}..${f[1]}}.txt,b{1..2}/file{${f[2]}..${f[3]}}.txt,b5/file-s{${f[4]}..${f[5]}}.log,cc/res-{${f[6]}..${f[7]}}.tmp}
}

function madeFilesCounters(){
    START=$((1+ ($RANDOM % 20)))

    local COUNT=$(($RANDOM % 10))

    END=$(($START + ($COUNT - 1)))
}

function madeFilesDirsCounters() {
    local i=0
    f=()
    while [ $i -le 3 ]
    do
        madeFilesCounters
        f+=( $START $END )
        i=$(($i + 1))
    done
}

function prepareDirStructure() {
    destroyDirStructure
    madeDirStructure
    madeFilesDirsCounters
    fillDirStructure
}

prepareDirStructure

COUNT_FILE_NAME="count.file"

printf "$(date +"%d-%m-%Y:%T")\t" >> $COUNT_FILE_NAME && find */ -type f  -not -path "cc/*" | wc -l >> $COUNT_FILE_NAME

FILES_COUNTER=0
RECORD_COUNTER=0

for filesQty in $(cut -f 2 count.file)
do
    FILES_COUNTER=$(($FILES_COUNTER + $filesQty))
    RECORD_COUNTER=$(($RECORD_COUNTER + 1))
done

AVGFILES=$(($FILES_COUNTER / $RECORD_COUNTER))

cat $COUNT_FILE_NAME
echo "Average files for all time is: $AVGFILES"

