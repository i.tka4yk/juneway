#!/bin/bash

function destroyDirStructure() {
    rm -r {a{1..2},b1} 2>/dev/null
}

function madeDirStructure() {
    mkdir -p {a1/11/{1,11},a2/22/{2,22,222},b1/33/3}
}

function fillDirStructure() {
    find */ -type d -exec bash -c 'echo "228.com 228.ru" > {}/a.log' \;
}

function prepareDirStructure() {
    destroyDirStructure
    madeDirStructure
    fillDirStructure
}

echo "Prepairing dirrectory & files structure"
prepareDirStructure

echo "Make substitution"
find */ -type f -exec sed -i 's/228\.com/229\.com/g' {} \;
echo "Done!"
