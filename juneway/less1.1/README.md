## Поднять машину с CentOS, установить docker и docker-compose

### Описание задания

[Install Docker on CentOS](https://docs.docker.com/engine/install/centos/)

[Install Docker Compose](https://docs.docker.com/compose/install/)

[Приложение джанго для деплоя](https://gitlab.com/chumkaska1/django_blog.git)

**Задача**: написать докер композ, в котором будет 3 сервиса:
- nginx
- postgresql
- app (само приложение) (докер файл с инсталяцией зависимостей)

nginx port 8000:8000

Создать нетворк для них

**Ресурсы**

[Django development with docker compose and machine](https://realpython.com/django-development-with-docker-compose-and-machine/) ( смотреть только как пример docker-compose !)

[Netwoking in Docker Compose](https://docs.docker.com/compose/networking/)


Ссылку на гит репу в гилабе где будет приложение + докерфайл и докер композ


Присоздании экземпляра ВМ в облаке по дефолту открыт только 22 порт в зависимости о выбранного облака необходимо в разделе правил фаервола открыть 8000 порт для всех Айпи адресов 0.0.0.0\0.

---

- django_blog - contains project for deplow with Dockerfile and docker-compose.yaml
- iac         - contains Terraform and Ansible configs

**HOW IT'S WORKS?**

Terraform creates gcp one compute instance and firewall rule for apply tcp:8000 port for nginx ingress.
Then after instance created Ansible provisioning OS/ It install some packets for general purpose, then install Docker and Docker Compose/
After that Ansible copying project dirrectory to instance and start docker-compose.
Terrafor in the end returning IP address of instance.

All what we need is fill "vars.tf" and start terraform apply in "iac" dirrectory and wait for process ending and return instance IP.
Then in browser follow link http://<instance IP>:8000.

We will use GCP for this task

***PREREQUISITEs:***
1. Installed Terraform
2. Installed Ansible
3. Installed Google Cloud SDK
4. gcs bucket for Terraform backend

You need to have a public and private keys for comunicate with GCP compute instance by Ansible

First of all - create private and public keys:

    ssh-keygen -t rsa -f ~/.ssh/id_rsa_gcloud -C irop.tka4yk@gmail.com -b 2048

Install Google Cloud sdk (we needs gcloud util)

    sudo echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
    sudo curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
    sudo apt-get update
    sudo apt-get install google-cloud-sdk

Authorize in GCP

    gcloud auth login
    gcloud auth application-default login

Setting project-id for gcloud util

    gcloud config set project skilful-wording-335119

Some useful command for obtain instances and os images lists

    gcloud compute instances --help
    gcloud compute instances list
    gcloud compute images list

How to manually add your public key to project level on GCP for OSLogin
In this repo we used Terrafor for it. But I thik it's useful to know about it.

    gcloud compute os-login ssh-keys add --key-file=/home/igor/.ssh/id_rsa_gcloud.pub

how to manually activate OSLogin in your project. As a previos step we used Terraform for it.

    gcloud compute project-info add-metadata --metadata enable-oslogin=TRUE

You need to fell your own values in vars.tf file before start terraform apply command

In "iac" dirrectory of this repo you can execute

- terraform init - for initalize Terraform, install appropriate provider for GCP
- terraform plan - show which resources terraform create in GCP
- terraform apply - Directly create resources on GCP in your account, in selected prohect
- terraform destroy - if you will wont do destroy all resources which was created in previos steps


