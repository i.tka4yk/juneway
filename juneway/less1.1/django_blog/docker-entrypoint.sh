#!/bin/bash

echo "Apply database migrations"
python manage.py migrate

# Start server
echo "Starting server"
#python manage.py runserver 0.0.0.0:8000
/usr/local/bin/gunicorn Blog.wsgi:application -w 2 -b :8000

