data "google_client_openid_userinfo" "me" {
}

resource "google_os_login_ssh_public_key" "cache" {
  user    = data.google_client_openid_userinfo.me.email
  key     = file(var.pub_key)
  project = "${var.project_id}"
}

resource "google_compute_project_metadata" "default" {
  metadata = {
    enable-oslogin = "TRUE"
  }
}
