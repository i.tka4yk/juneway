variable "node_count" {
  default = "1"
}

resource "google_compute_instance" "vm_instance" {
  count = "${var.node_count}"

  name                      = "node${count.index + 1}"
  machine_type              = "e2-small"
  allow_stopping_for_update = "true"
  zone                      = "us-central1-a"
  tags                      = ["web"]

  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7"
      size  = "20"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }

  provisioner "remote-exec" {
    inline = ["sudo yum update", "sudo yum install python3 -y", "echo Done!"]

    connection {
      host        = self.network_interface.0.access_config.0.nat_ip
      type        = "ssh"
      user        = "${var.gc_user}"
      private_key = file(var.pvt_key)
    }
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u irop_tka4yk_gmail_com -i '${self.network_interface.0.access_config.0.nat_ip},' --private-key ${var.pvt_key} -e 'pub-key=${var.pub_key}' app-install.yml"
  }
}

resource "google_compute_firewall" "rules" {
  name    = "project-allow-django"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["8000"]
  }

  source_ranges = ["0.0.0.0/0"]
}

output "vm_instance_ip_adresses" {
  value = {
    for vm in google_compute_instance.vm_instance:
    vm.name => vm.network_interface.0.access_config.0.nat_ip
  }
}
