## Nginx with Lua

It's Docker file for build image with Nginx web server which support LUA scripting in its config file

Use **sudo docker build -t my-lua-nginx:v1.0 .** for build image

Use **sudo docker run -d --rm --name my-lua-nginx -p 8080:80 my-lua-nginx:v1.0** for start Nginx web-server container on localhost 8080 port
Or use **sudo docker run -d -p 8080:80 --rm --mount type=bind,src="$(pwd)"/nginx.conf,dst=/usr/local/nginx/conf/nginx.conf  my-lua-nginx:v1.0** for mount local Nginx config to container

Use **curl http://localhost:8080/hello** for test Lua block in Nginx config

File nginx.conf consists a lua init block for activate lua and lua block in location **/hello** section for test lua
