#!/bin/bash

docker network create some-network
docker volume create some-docker-certs-ca
docker volume create some-docker-certs-client

docker run --rm --privileged --name some-docker -d --network some-network --network-alias docker -e DOCKER_TLS_CERTDIR=/certs -v some-docker-certs-ca:/certs/ca -v some-docker-certs-client:/certs/client docker:dind

sleep 3s

docker run -it --rm --mount type=bind,src="$(pwd)"/Dockerfile,dst=/root/Dockerfile --network some-network -e DOCKER_TLS_CERTDIR=/certs -v some-docker-certs-client:/certs/client:ro -v "$(pwd)"/dimg:/dimg docker:latest sh -c "\
cd ~ && \
docker build -t my-nginx-alpine . && \
docker save -o /dimg/my-nginx-alpine.tar my-nginx-alpine\
"

