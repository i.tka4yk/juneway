inspired by https://github.com/jpetazzo/dind

Build the image:
```bash
docker build -t dind .
```

Run Docker-in-Docker and get a shell where you can play, and docker daemon logs
to stdout:
```bash
docker run --privileged -t -i dind
```
