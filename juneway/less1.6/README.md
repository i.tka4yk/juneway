**Описание задания**

Напишите Докер компоСТ (docker-compose.yml со сл условиями:
1. Три контейнера nginx1 nginx2 grafana (https://hub.docker.com/r/grafana/grafana)
2. Nginx1 port 8001 на Grafana
2. Nginx2 port 8002 на Grafana
3. Grafana не пробрасывать ничего наружу. Она не должна подниматься если вы её стопорите руками.
4. Nginx1 и Nginx2 в разных сетях.
5. И конечно 2 конфига нжинкса которые будут пробрасывать на Grafana (пример конфиг с лес1.1 джанго конф)

Ответ в Гитлаб

*Задача со зведочкой*

Сделать хелсчек Nginx1 в контейнере с Grafana
Уронить Nginx1 посмотреть что будет) Скрины в ответ на задачу. Либо пастбин.

---

Here's how I kill the Nginx1 container

![Kill Nginx1](./assets/kill_nginx1.png)

Compose reaction on Nginx 1 kill]

![Compose reaction on Nginx 1 kill](./assets/kill_nginx_compose_reaction.png)

Get grafana container status by command]

![Get grafana container status by command](./assets/inspect_grafana_container_after_nginx_kill.png)

Get grafana container status by command

![Grafana container healthy status](./assets/grafana_status.png)

