**Описание задания**

Напишите Докер компоСТ (docker-compose.yml со сл условиями:
1. Три контейнера nginx1 nginx2 grafana (https://hub.docker.com/r/grafana/grafana)
2. Nginx1 port 8001 на Grafana
2. Nginx2 port 8002 на Grafana
3. Grafana не пробрасывать ничего наружу. Она не должна подниматься если вы её стопорите руками.
4. Nginx1 и Nginx2 в разных сетях.

Иногда контейнерам нужен общий волум в режиме рид онли.
Модифицируем docker-compose.yml таким образом, чтоб не копировать конфиги каждому контейнеру, а создать в композе волюм и подключить его к 2м нжинксам.

---
For start this lesson you need execute next commands in lesson dirrectory
1. sudo docker-compose up

---
**Useful links**
[Define named volume with host mount in the docker compose file](https://blog.code4hire.com/2018/06/define-named-volume-with-host-mount-in-the-docker-compose-file/)
