 **Описание задания**

1. Создать пользователя, назвать как нравится. Разрешить ему выполнять через sudo только одну программу (skukozh из вложения)
2. Выставить атрибуты на skukozh:
   - владелец: чтение, исполнение
   - группа: чтение, исполнение
   - мир: -
3. Скопировать любой исполняемый файл (ls, bash, итд) в домашний каталог пользователя.
4. Скукожить этот исполняемый файл, и убедиться, что он не работает ($ sudo skukozh ./bash , например).

---

Create user

![create user](./assets/01_create_user.png)

Get skukozh

![get skukozh](./assets/02_get_skukozh.png)

chmod

![chmod](./assets/03_chmod.png)

Try to run skukozh by regular user after change perms

![try to run skukozh](./assets/04_skukozh_try_to_run.png)

Copy ls to user home dirrectory and make shure it work

![cp ls and make shur it works](./assets/05_cp_ls_in_homedir_and_make_shure_it_work.png)

Make sudoerrs file for aur task

![make sudoers](./assets/06_sudoerrs_for_user.png)

Try sudo by user which mentioned in sudoerrs file

![try sudo by user](./assets/07_try_sudo_by_user.png)
