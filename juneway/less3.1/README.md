## Less 3.1 (Ansible)

####  Описание задания

1. Создать 2 виртуалки в облаке
2. Открыть 80 порт
3. Написать Инвентори где будут эти 2 хоста в ямл формате в группе web-srv
4. Написать простенький плейбук который установит nginx и запустит его
5. Плейбук должен распространяться только на группу web-srv

---

Google Cloud Compute instances makes with Terraform

* _inventories_ folder consist some structire for prod environment
* _nginx-install.yml_ it`s simple Ansibles playbook with Ngix web server instalation_

_command for start playbook_
```
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u irop_tka4yk_gmail_com -i ./inventories/prod/ --private-key ~/.ssh/google_compute_engine nginx-install.yml
```
