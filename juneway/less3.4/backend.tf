terraform {
  backend "gcs" {
    bucket = "tf-state-juneway"
    prefix = "terraform/state"
  }
}

