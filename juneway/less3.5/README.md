## Less 3.1 (Ansible)

#### Описание задания

1. Создать 2 виртуалки в облаке
2. Открыть 80 порт
3. Написать Инвентори где будут эти 2 хоста в ямл формате в группе web-srv
4. Написать простенький плейбук который установит nginx и запустит его
5. Плейбук должен распространяться только на группу web-srv

---

Google Cloud Compute instances makes with Terraform

- _inventories_ folder consist some structire for prod environment
- _nginx-install.yml_ it`s simple Ansibles playbook with Ngix web server instalation\_

_command for start playbook_

```
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u irop_tka4yk_gmail_com -i ./inventories/prod/ --private-key ~/.ssh/google_compute_engine nginx-install.yml
```

## Less 3.2 (Ansible)

1. Модифицировать плейбук из прошлого задания, что бы устанавливался не только а nginx, а ещё vim, nmap.
2. Пакеты должны быть списком (list)

## Less 3.3 (Ansible)

1. Модифицировать плейбук из прошлого урока в роль
2. Сделать темплейт конфига нжингс (джинжа2)

Переменные - server_name == test1.juneway.pro test2.juneway.pro

Должен быть один шаблон на 2 виртуальных хоста.
( Сервер1 = test1..... Сервер2 = test2...)

Конфиг копировать по стандартному пути нжингса.

**Сам конфиг**

```
server {
  listen 80;
  listen [::]:80;
  server_name ваша_переменная;
  root /var/www/ваша_переменная;
  index index.html;
  try_files  /index.html;
}
```

Переменные должы храниться в варс файле роле

Итог - 2 конфига на каждом из серверов.

**Полезные ресурсы**

- [Высвобождение всей мощи Jinja2](http://onreader.mdl.ru/MasteringAnsible2nd/content/Ch03.html)
- [Templating Jinja2](https://docs.ansible.com/ansible/latest/user_guide/playbooks_templating.html)
- [Ansible Roles](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html)

## Less 3.4 (Ansible)

Добавить в шаблон с прошлого задания upstreams которые беруться из переменных

```
upstream:
  upstream1:
   server: "127.0.0.1:8002"
  upstream2:
   server: "127.0.0.1:8003"
  upstream3:
   server: "127.0.0.2:8004"
```

в конфиге должно быть так

```
upstream test {
  server 127.0.0.1:8002;
  server 127.0.0.1:8003;
  server 127.0.0.2:8004;
}
```

1. Они должны попасть на оба сервера. Сервер нэйм разные ,апстримы одинаковые
2. При добавлении нового апстрима я должен деплоить не меняя сам Шаблон конфига нжингса.

На двух серверах должно быть по 2 конфига test1.... test2....


## Less 3.5 (Ansible)

Модифицировать роль в прошлого задания
1. Добавить Юзера на каждый из серверов: Name: "ansible", uid: 1005, gid: 1005.
2. Сменить Имя серверов на test1 test2 соответственно файлу inventory.
3. Изменить файл hosts - добавить записи об узлах, которые обрабатываются ролью
4. Модулем setup вывести на экран ip настройки ваших удвлённых серверов

---

**Решение задания 4**

```
ANSIBLE_HOST_KEY_CHECKING=False ansible all -m setup -a "filter=*ipv4*" -u irop_tka4yk_gmail_com -i ./inventories/prod/ --private-key ~/.ssh/google_compute_engine
```
