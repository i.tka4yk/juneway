resource "google_compute_firewall" "rule-nginx" {
  name    = "allow-nginx"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["80", "433"]
  }

  source_ranges = ["0.0.0.0/0"]
}

