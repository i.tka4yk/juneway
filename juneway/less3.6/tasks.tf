variable "node_debian_count" {
  default = "1"
}

variable "node_centos_count" {
  default = "1"
}

resource "google_compute_instance" "vm_centos_instance" {
  count = "${var.node_centos_count}"

  name                      = "node-centos-${count.index + 1}"
  machine_type              = "e2-small"
  allow_stopping_for_update = "true"
  zone                      = "us-central1-a"

  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7"
      size  = "20"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }

 connection {
   host        = self.network_interface.0.access_config.0.nat_ip
   type        = "ssh"
   user        = "${var.gc_user}"
   private_key = file(var.pvt_key)
 }

  provisioner "remote-exec" {
    inline = ["sudo yum install python3 -y", "echo Done!"]
  }
}

resource "google_compute_instance" "vm_debian_instance" {
  count = "${var.node_debian_count}"

  name                      = "node-debian-${count.index + 1}"
  machine_type              = "e2-small"
  allow_stopping_for_update = "true"
  zone                      = "us-central1-a"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
      size  = "20"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }

 connection {
   host        = self.network_interface.0.access_config.0.nat_ip
   type        = "ssh"
   user        = "${var.gc_user}"
   private_key = file(var.pvt_key)
 }

  provisioner "remote-exec" {
    inline = ["sudo apt install python3 -y", "echo Done!"]
  }
}

resource "null_resource" "erase_host_list" {
  provisioner "local-exec" {
    command = "echo '' >> ./host.list"
  }
}

resource "null_resource" "write_eci_centos_ip_addresses" {
  depends_on = [google_compute_instance.vm_centos_instance,]

  provisioner "local-exec" {
    command = "echo '${join("\n", formatlist("projectId =%v, hoostname=%v ; instance=%v ; private=%v ; public=%v", data.google_project.project.number, google_compute_instance.vm_centos_instance.*.name, google_compute_instance.vm_centos_instance.*.instance_id, google_compute_instance.vm_centos_instance.*.network_interface.0.network_ip, google_compute_instance.vm_centos_instance.*.network_interface.0.access_config.0.nat_ip))}' >> ./host.list"
  }
}

resource "null_resource" "write_eci_debian_ip_addresses" {
  depends_on = [google_compute_instance.vm_debian_instance,]

  provisioner "local-exec" {
    command = "echo '${join("\n", formatlist("projectId =%v, hoostname=%v ; instance=%v ; private=%v ; public=%v", data.google_project.project.number, google_compute_instance.vm_debian_instance.*.name, google_compute_instance.vm_debian_instance.*.instance_id, google_compute_instance.vm_debian_instance.*.network_interface.0.network_ip, google_compute_instance.vm_debian_instance.*.network_interface.0.access_config.0.nat_ip))}' >> ./host.list"
  }
}
