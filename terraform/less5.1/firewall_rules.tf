resource "google_compute_firewall" "rules" {
  name    = "allow-nginx"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  source_ranges = ["0.0.0.0/0"]
}