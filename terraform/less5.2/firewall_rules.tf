resource "google_compute_firewall" "rule-nginx" {
  name    = "allow-nginx"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["80", "433"]
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "rule-udp" {
  name    = "allow-udp"
  network = "default"

  allow {
    protocol = "udp"
    ports    = ["10000-20000"]
  }

  source_ranges = ["10.0.0.23/32"]
}
