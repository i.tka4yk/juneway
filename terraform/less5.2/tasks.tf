variable "node_count" {
  default = "3"
}

resource "google_compute_instance" "vm_instance" {
  count = "${var.node_count}"

  name                      = "node-${count.index + 1}"
  machine_type              = "e2-small"
  allow_stopping_for_update = "true"
  zone                      = "us-central1-a"

  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7"
      size  = "20"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }

 connection {
   host        = self.network_interface.0.access_config.0.nat_ip
   type        = "ssh"
   user        = "${var.gc_user}"
   private_key = file(var.pvt_key)
 }

 provisioner "remote-exec" {
  inline = [
    "sudo yum update",
    "sudo yum install -y nginx",
    "sudo systemctl enable nginx",
    "echo \"${file(var.nginx_index_path)}\" | sudo tee /usr/share/nginx/html/index.html > /dev/null",
    "sudo sed -i \"s/host_ip/${self.network_interface.0.network_ip}/g\" /usr/share/nginx/html/index.html",
    "sudo sed -i \"s/host_name/${self.name}/g\" /usr/share/nginx/html/index.html",
    "sudo systemctl start nginx"
  ]
 }
}

resource "null_resource" "write_eci_ip_addresses" {
  depends_on = [google_compute_instance.vm_instance,]

  provisioner "local-exec" {
    command = "echo '${join("\n", formatlist("projectId =%v, hoostname=%v ; instance=%v ; private=%v ; public=%v", data.google_project.project.number, google_compute_instance.vm_instance.*.name, google_compute_instance.vm_instance.*.instance_id, google_compute_instance.vm_instance.*.network_interface.0.network_ip, google_compute_instance.vm_instance.*.network_interface.0.access_config.0.nat_ip))}' > ./host.list"
  }
}
