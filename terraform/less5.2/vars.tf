variable "pub_key" {
  type    = string
  default = "/home/igor/.ssh/id_rsa_gcloud.pub"
}

variable "project_id" {
  type    = string
  default = "skilful-wording-335119"
}

variable "nginx_index_path" {
  type    = string
  default = "nginx/index.html"
}

variable "gc_user" {
  type    = string
  default = "irop_tka4yk_gmail_com"
}

variable "pvt_key" {
  type    = string
  default = "/home/igor/.ssh/id_rsa_gcloud"
}
