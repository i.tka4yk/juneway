resource "cloudflare_record" "IgorTka4yk" {
  zone_id = "${var.cf_zone_id}"
  name    = "igortka4yk.juneway.pro"
  value   = google_compute_address.less53_addr.address
  type    = "A"
  ttl     = 3600
}
