resource "google_compute_firewall" "fw-rule-less53" {
  name    = "allow-l53-ports"
  network = "default"

/*
  allow {
    protocol = "tcp"
    ports    = ["80", "8001", "8802"]
  }
*/

  dynamic "allow" {
    iterator = ports
    for_each = var.fw_allow_ports
    content {
      protocol = "tcp"
      ports    = [ports.value]
    }
  }

  source_ranges = ["0.0.0.0/0"]
}

