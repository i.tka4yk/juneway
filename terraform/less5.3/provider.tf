terraform {
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}

provider "google" {
  project    = "${var.project_id}"
  region     = "us-central1"
  zone       = "us-central1-a"
}

provider "cloudflare" {
  email = "${var.cf_email}"
  api_key = "${var.cf_api_key}"
  account_id = "${var.cf_account_id}"
}
