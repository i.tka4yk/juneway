variable "node_count" {
  default = "1"
}

resource "google_compute_address" "less53_addr" {
  name = "ipv4-addr-for-less53"
}

resource "google_compute_disk" "disk-8g" {
  name = "disk-8g"
  type = "pd-standard"
  size = 8
  zone = "${var.google_zone}"
}

resource "google_compute_disk" "disk-10g" {
  name = "disk-10g"
  type = "pd-standard"
  size = 10
  zone = "${var.google_zone}"
}

resource "google_compute_instance" "vm_instance" {
  count = "${var.node_count}"

  name                      = "node-${count.index + 1}"
  machine_type              = "e2-small"
  allow_stopping_for_update = "true"
  zone                      = "${var.google_zone}"

  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7"
      size  = "20"
    }
  }

  attached_disk {
    source = google_compute_disk.disk-8g.self_link
    device_name = "disk-8g"
  }

  attached_disk {
    source = "disk-10g"
    device_name = "disk-10g"
  }

  network_interface {
    network = "default"
    access_config {
      nat_ip = google_compute_address.less53_addr.address
    }
  }

 connection {
   host        = self.network_interface.0.access_config.0.nat_ip
   type        = "ssh"
   user        = "${var.gc_user}"
   private_key = file(var.pvt_key)
 }

 provisioner "remote-exec" {
  inline = [
    "sudo yum install -y yum-utils",
    "sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo",
    "sudo yum install -y docker-ce docker-ce-cli containerd.io",
    "sudo systemctl enable docker.service",
    "sudo systemctl enable containerd.service",
    "sudo systemctl start docker",
    "sudo docker run -d --name nginx --restart always -p 8001:80 nginx",
    "sudo docker exec nginx sh -c 'echo Juneway ${self.name} ${google_compute_address.less53_addr.address} > /usr/share/nginx/html/index.html'"
  ]
 }
}

resource "null_resource" "write_eci_ip_addresses" {
  depends_on = [google_compute_instance.vm_instance,]

  provisioner "local-exec" {
    command = "echo '${join("\n", formatlist("projectId =%v, hoostname=%v ; instance=%v ; private=%v ; public=%v", data.google_project.project.number, google_compute_instance.vm_instance.*.name, google_compute_instance.vm_instance.*.instance_id, google_compute_instance.vm_instance.*.network_interface.0.network_ip, google_compute_instance.vm_instance.*.network_interface.0.access_config.0.nat_ip))}' > ./host.list"
  }
}
